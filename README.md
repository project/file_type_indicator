File type indicator text filter is an input filter that adds small icons 
to file links, depends on its extension.

Installation
============

1. Enable File type indicator filter module in the Drupal admin.
2. Configure your Text Format to enable the filter "Add icon to file link, 
   depends on its extension".
3. Configure filter settings. Set the comma separated list of file types 
   to handle.
4. Save Text Format settings
