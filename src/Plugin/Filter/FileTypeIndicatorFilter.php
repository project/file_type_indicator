<?php

namespace Drupal\file_type_indicator\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\Html;

/**
 * Provides a filter that adds icons to file links, depends on its extension.
 *
 * @Filter(
 *   id = "file_type_indicator_filter",
 *   title = @Translation("Add icon to file link, depends on its extension"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "file_types" = "pdf,doc,zip"
 *   }
 * )
 */
class FileTypeIndicatorFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form['file_types'] = [
      '#type' => 'textarea',
      '#title' => $this->t('File types to process'),
      '#description' => $this->t('Add file extensions, separated by comma.'),
      '#default_value' => $this->settings['file_types'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $file_types = $this->settings['file_types'] ? explode(',', $this->settings['file_types']) : [];
    if (empty($file_types)) {
      return $result;
    }
    $dom = Html::load($text);
    $links = $dom->getElementsByTagName('a');
    foreach ($links as $a) {
      $href = $a->getAttribute('href');
      if (!$href) {
        continue;
      }
      // This is file link.
      $clear_path = parse_url($href, PHP_URL_PATH);
      if ($clear_path && ($check_result = pathinfo($clear_path, PATHINFO_EXTENSION))) {
        if (in_array($check_result, $file_types)) {
          $file_type_class = 'fiv-cla fiv-icon-' . $check_result;
          $a->setAttribute('class', ($a->getAttribute('class') ? $a->getAttribute('class') . ' ' . $file_type_class : $file_type_class));
        }
      }
    }
    $result->setProcessedText(Html::serialize($dom))
      ->addAttachments(['library' => ['file_type_indicator/icons_css']]);

    return $result;
  }

}
